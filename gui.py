# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import codecs

import numpy as np
from useful import polish_letters, split_up_sentence, morf, first_lemma, analyse_word
from tkinter import *
from functools import partial
import pickle
import json
import random

# Ignorowanie FutureWarnings od numpy w konsoli
import warnings

warnings.filterwarnings('ignore', category=FutureWarning)
import webbrowser
from keras.models import load_model

# Początkowe
model = load_model('chatbot_model.h5')

intents = json.loads(codecs.open('intents.json').read())
words = pickle.load(codecs.open('words.pkl', 'rb'))
classes = pickle.load(codecs.open('classes.pkl', 'rb'))
list_of_intents = intents['intents']

# GUI
first_message = True
current_context = ["beginning"]
videos = ["https://www.youtube.com/watch?v=149tYQEhqvY&ab_channel=TheMindfulMovement",
          "https://www.youtube.com/watch?v=6JzMZUlI1PU&ab_channel=TheMindfulMovement",
          "https://www.youtube.com/watch?v=l5crs-lCmGc&ab_channel=TheMindfulMovement"]
exercises = ["exerc1"]
ex_dict = {"exerc1": "ćwiczenie oddechowe"}


def bow(sentence, words, show_details=True):
    # tokenizacja
    sentence_words = split_up_sentence(sentence)
    # macierz słów z reprezentacją 0, 1
    bag = [0] * len(words)
    for s in sentence_words:
        for i, w in enumerate(words):
            if w == s:
                # 1 jeśli słowo jest, 0 jeśli nie
                bag[i] = 1
                if show_details:
                    print("found in bag: %s" % w)
    return np.array(bag)


def predict_class(sentence, model, words, classes):
    p = bow(sentence, words, show_details=False)
    res = model.predict(np.array([p]))[0]
    results = [[i, r] for i, r in enumerate(res)]
    # sortowanie ze względu na prawdopodobieństwo
    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []
    for r in results:
        return_list.append({"intent": classes[r[0]], "probability": str(r[1])})
    return return_list


def get_response(ints, intents_json):
    global current_context
    list_of_intents = intents_json['intents']
    for candidate in ints:
        for i in list_of_intents:
            if i['tag'] == candidate['intent']:
                if current_context:
                    if bool(set(current_context) & set(i['context_now'])):
                        if i['next_context']:
                            current_context = i['next_context']
                        else:
                            current_context.clear()
                        result = random.choice(i['responses'])
                        return result
                else:
                    result = random.choice(i['responses'])
                    return result
    result = random.choice(list_of_intents[-1]['responses'])  # odpowiedz, gdy chatbot nie rozumie
    return result


exchange_capitalise = {'ja': 'ty',  'mój': 'twój'}  # Wielka litera z szacunku do rozmówcy
exchange = {'ja': 'ty',  'mój': 'twój', 'ty': 'ja', 'twój': 'mój', 'jeżeli': 'że', 'gdyby': 'że', 'jeśli': 'że'}
first_to_remove = ['a', 'ale', 'co', 'czemu', "jak", "pamiętam", "tak", "bo", "ponieważ", "dlatego"]


def empathic_response(sentence, ints, intents_json):
    result = ""

    # predykcja intencji
    list_of_intents = intents_json['intents']
    for candidate in ints:
        for i in list_of_intents:
            if i['tag'] == candidate['intent']:
                if i['context_now']:  # kontekst musi być == empathy
                    if 'empathy' in i['context_now']:
                        result = random.choice(i['responses'])
                        global current_context
                        current_context = i['next_context']
                        break
        if result != "":
            break
    if result == "":
        result = polish_letters(random.choice(list_of_intents[-1]['responses']))  # odpowiedz, gdy chatbot nie rozumie
    else:
        result = polish_letters(result)

        if "[]" not in result:
            return result

        sentence = sentence.lower()

        if sentence[0:4] == 'nie, ':
            sentence.remove('nie, ')
        if sentence[0:4] == 'tak, ':
            sentence.remove('tak, ')

        # przebudowa
        sentence_words = sentence.split()
        if sentence_words[0].lower() in first_to_remove:
            sentence_words = sentence_words[1:]

        if "[no]" in result:
            sentence_words.remove('nie')
            result.replace('[no]', "[]")

        sentence_analysed = {word: analyse_word(word) for word in sentence_words}
        changed = ""

        for word in sentence_words:
            new_word_whole = ''

            # dla kazdego slowa z oryginalnego zdania przeprowadzana jest przebudowa
            for tag in sentence_analysed[word]:  # jezeli zlozone slowo jak np. "byli-by-smy"
                tagset = tag[1]
                new_tagset = ""
                # zamiana 1 i 2 osoby
                if ':sec' in tagset:
                    new_tagset = tagset.replace(':sec', ':pri')
                elif ':pri' in tagset:
                    new_tagset = tagset.replace(':pri', ':sec')

                # zamiana lematow slow oryginalnie nacechowanych  1-szo lub 2-go osobowo
                new_word = tag[0]
                if new_tagset == "":
                    new_tagset = tagset
                else:
                    if new_word in exchange:
                        new_word = exchange[new_word]

                id_res = morf.getTagId(new_tagset)
                new_word_whole = new_word_whole + morf.generate(new_word, id_res)[0][0]

            if first_lemma(word) in exchange_capitalise:
                new_word_whole = new_word_whole.capitalize()
            changed = changed + ' ' + new_word_whole
        result = result.replace('[]', changed)
        result = result.capitalize()
    return result


def chatbot_response(msg, model, intents, words, classes):
    ints = predict_class(msg, model, words, classes)
    if 'empathy' in current_context[0]:
        res = empathic_response(msg, ints, intents)
    else:
        res = get_response(ints, intents)
    res = polish_letters(res)
    return res


class HyperlinkManager:
    def __init__(self, text):
        self.text = text
        self.text.tag_config("hyper", foreground="blue", underline=1)
        self.text.tag_bind("hyper", "<Button-1>", self._click)
        self.links = {}

    def add(self, action):
        tag = "hyper-%d" % len(self.links)
        self.links[tag] = action
        return "hyper", tag

    def _click(self, event):
        for tag in self.text.tag_names(CURRENT):
            if tag[:6] == "hyper-":
                self.links[tag]()
                return


set_FM = ["exerc_ok", "videos", "exerc1", "exerc1_1", "exerc1_2", "exerc1_3", "exerc1_4", "exerc1_5"]


def send(event=None):
    global current_context
    print(current_context)
    global first_message
    if not entry_box.edit_modified() and not first_message:
        return
    entry_box.edit_modified(False)

    msg = entry_box.get("1.0", 'end-1c').strip()
    entry_box.delete("0.0", END)

    if msg != '' or first_message:
        history.config(state=NORMAL)
        history.config(foreground="saddle brown", font=("Times", 12))
        # print(current_context)

        # if msg != '':
        history.insert(END, "Ty: " + msg + '\n\n')

        if msg.lower() == "stop" or msg.lower() == 'koniec':
            res = polish_letters(random.choice(list_of_intents[-3]['responses']))
            current_context = ['ask_end']
        else:
            res = chatbot_response(msg, model, intents, words, classes)
            if current_context:
                if bool(set(current_context) & set(set_FM)):
                    first_message = True

                if current_context == ["exerc_ok"]:
                    ex = random.choice(exercises)
                    current_context = [ex]
                    history.insert(END, "Bot: Dzisiaj mam dla Ciebie " + ex_dict[ex] + '\n\n')
                    history.config(state=DISABLED)
                    history.yview(END)
                    return True

                if current_context == ["videos"]:
                    vid = random.choice(videos)
                    history.insert(END, "Bot: Wyślę linka do tutorialu na YouTube: ", INSERT, "Meditation Tutorial",
                                   hyperlink.add(partial(webbrowser.open, vid)),
                                   '\nJak skończysz kliknij "Wyślij" lub naciśnij "Enter" :D\n\n')
                    history.config(state=DISABLED)
                    history.yview(END)
                    return True
            else:
                first_message = False

            if "[whole]" in res:
                res = res.replace('[whole]', msg.lower())

            if res == "":
                res = polish_letters(random.choice(list_of_intents[-2]['responses']))

        history.insert(END, "Bot: " + res + '\n\n')
        history.config(state=DISABLED)
        history.yview(END)

    if current_context and current_context[0] == "end":
        entry_box.config(state=DISABLED)
        return True

    return True


base = Tk()
base.title("Twój Pomocnik :)")
base.geometry("500x700")
base.resizable(width=FALSE, height=FALSE)
base.configure(background="DarkGoldenrod")

# Stwórz okno
history = Text(base, bd=1, bg="antique white", height=8, width=100, font="Times", wrap=WORD)
history.config(state=DISABLED)

# Suwak
history_scrollbar = Scrollbar(base, command=history.yview, cursor="gumby", bg="antique white")
history_scrollbar.place(x=478, y=6, height=592)
history['yscrollcommand'] = history_scrollbar.set
history.place(x=6, y=6, width=474, height=592)

# Mozliwosc zamieszczenia linku
hyperlink = HyperlinkManager(history)

# Guzik do wysylania
send_button = Button(base, font=("Times", 14, 'bold'), text="Wyślij", width=12, height=5,
                     bd=1, bg="goldenrod", activebackground="goldenrod3", fg='saddle brown',
                     command=send)
send_button.place(x=374, y=604, height=90, width=120)

# Okno do wpisywania tekstu
entry_box = Text(base, bd=1, bg="antique white", width=29, height=5, font="Times",
                 highlightcolor="gold", highlightbackground="goldenrod", highlightthickness=2, fg='saddle brown')
entry_box.place(x=6, y=604, height=90, width=362)
bindtags = list(entry_box.bindtags())
bindtags.insert(2, "custom")
entry_box.bindtags(tuple(bindtags))
entry_box.bind_class("custom", "<Return>", send)  # Enter takze wysyla wiadomosc

base.mainloop()
