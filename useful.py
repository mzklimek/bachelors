# -*- coding: utf-8 -*-
import morfeusz2
morf = morfeusz2.Morfeusz()

# Przetwarzanie języka przez Morfeusza


def analysis(text: str) -> str:
    return morf.analyse(text)


def analyse_word(word: str):
    size = len(morf.analyse(word))
    if size > 1:
        if 'praet' in morf.analyse(word)[0][2][2] and 'praet' not in morf.analyse(word)[1][2][2]:
            return [(morf.analyse(word)[i][2][1], morf.analyse(word)[i][2][2]) for i in range(size)]
    return [(morf.analyse(word)[0][2][1], morf.analyse(word)[0][2][2])]


def first_lemma(text: str) -> str:
    # zwraca pierwszy lemat slowa w grafie

    lemma = analysis(text)[0][2][1]
    condition = lemma.find(':')
    # tylko dla rzeczownikow o różnych znaczeniach np. 'zamek'

    if condition != -1:
        lemma = lemma[:condition]
    return lemma


def polish_letters(text: str) -> str:
    text = text.replace("Ĺ›", "ś")
    text = text.replace("Ä‡", "ć")
    text = text.replace("Ĺ„", "ń")
    text = text.replace("Ä™", "ę")
    text = text.replace("Ăł", "ó")
    text = text.replace("Ä…", "ą")
    text = text.replace("Ä…", "ą")
    text = text.replace("Ĺ‚", "ł")
    text = text.replace("ĹĽ", "ż")
    text = text.replace("Ĺş", "ź")
    text = text.replace("Ĺš", "Ś")
    return text


def split_up_sentence(sentence):
    sentence_words = sentence.split()
    sentence_words = [first_lemma(word.lower()) for word in sentence_words]
    return sentence_words
